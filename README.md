# Temporal Stressing Fast Downward
---


## Getting started

This repository provides the Temporal Stressing Fast Downward (TSFD) planning system. This planner uses stressed actions and an improved planning procedure to find solutions to any problem expressible in PDDL 2.1. More information about TSFD can be found at [1].

### System requirements

TSFD uses CMAKE and bases on Temporal Fast Downward (TFD) [2]. Please make sure that [TFD](http://gki.informatik.uni-freiburg.de/tools/tfd/downloads.html) runs fine on your system. 
Next to the requirements of TFD, TSFD needs the following packages:

- Ubuntu 14.04 Trusty
- CMake (>= 2.8.12)
- C++11, or greater

### Installation instructions

Create a new directory where you will install all the source code for TSFD. We suggest $HOME/TSFD. This directory will be referred to in the rest of this document as $TSFD. First, add the $TSFD environment variable to your bashrc. 
```
# add the following to your $HOME/.bashrc file
export TSFD=$HOME/TSFD  # Or whatever installation path you chose
```

Resource your .bashrc:
```
source $HOME/.bashrc
```

Create the source folder for the TSFD planning system and clone the repository.
```
mkdir $TSFD
git clone https://gitlab.com/h2t/TSFD-Planner.git $TSFD
```

Enter the source folder and create a new folder ``build`` inside the ``TSFD``-folder:
```
cd $TSFD
mkdir build
```

Enter the newly created folder:
```
cd build
```

Run CMake, using the ``CMakeLists.txt`` file of the root folder:
```
cmake ..
```

This step should create a makefile inside the ``build``-folder.  You can build TSFD using:
```
make -j8
```
Adjust the number of the argument ``-j8`` to the amount of available cores of your system.


## Usage

There are two ways to use TSFD. The first uses CMake, to integrate TSFD to some other program or framework, such as ArmarX[3]. Moreover, TSFD provides a binary which can be executed directly from the command line. 

### Integration to other frameworks

CMake is the recommend way to integrate TSFD to some other piece of software. The other program must include TSFD using the following dependency in your ``CMakeLists.txt``
```
find_package(TemporalStressingFastDownward REQUIRED)
```

This should add all necessary paths to your program. After that you can access the header files of TSFD using:
```
#include <TemporalStressingFastDownward/some_header_file.h>
```

All header and source files can be found at.
```
$TSFD/TemporalStressingFastDownward/
```

### Using the binary file

TSFD also offers a binary file. This file is accessible after running ``make`` as described in the installation instructions. You can call the binary file with the following command:
```
./$TSFD/build/bin/TemporalStressingFastDownwardExecutable
```

This command runs TSFD with default parameters. These parameters can be changed using the ``main.cpp`` file. 

By default, TSFD uses the following two PDDL files:
```
/tmp/pddl2sas_input_domain.pddl
/tmp/pddl2sas_input_problem.pddl
```

Normally, ArmarX[3] creates these PDDL files. You can simply modify the ``main.cpp`` file to use other PDDL description files or other parameters. 


## References

[1] F. Peller, M. Wächter, M. Grotz, P. Kaiser and T. Asfour, "Temporal concurrent planning with stressed actions", 2018.

[2] P. Eyerich, R. Mattmüller and G. Röger, "Using the Context-enhanced Additive Heuristic for Temporal and Numeric Planning", 2009. 

[3] N. Vahrenkamp, M. Wächter, M. Kröhnert, K. Welke and T. Asfour, “The robot software framework armarx”, 2015.


