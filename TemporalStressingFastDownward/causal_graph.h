
#pragma once

#include <iosfwd>
#include <vector>
#include <set>
#include <map>


namespace tsfd {

class CausalGraph
{
        std::vector<std::vector<int> > arcs;
        std::vector<std::vector<int> > edges;
    public:
        CausalGraph(std::istream &in);
        ~CausalGraph()
        {}

        const std::vector<int> &get_successors(int var) const;
        const std::vector<int> &get_neighbours(int var) const;
        void get_comp_vars_for_func_var(int var, std::vector<int>& comp_vars);
        void get_functional_vars_in_unrolled_term(int top_var, std::set<int>& intermediate_vars);
        void dump() const;
};
}
