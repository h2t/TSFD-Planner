//
//  temporal_stressing_planner_controller.cpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#include "temporal_stressing_planner_controller.h"


using namespace std;
using namespace tsfd;

tsfd::TemporalStressingPlannerController::TemporalStressingPlannerController() :
            domain(""), useProblem(""), bestMakespan(HUGE_VAL), bestCost(HUGE_VAL), nextPlanStep(0)
{
    cout << "Initializing Planner Controller" << endl;
    srand(1);
}

void tsfd::TemporalStressingPlannerController::reset()
{
    cout << "Resetting Planner Controller" << endl;
    this->currentState = TimeStampedState();
    this->plan.clear();
    this->nextPlanStep = 0;
    this->bestCost = HUGE_VAL;
    this->bestMakespan = HUGE_VAL;
    reset_everything();
}

// ATTENTION, it's possible to add elements with same key
string tsfd::TemporalStressingPlannerController::getPlannerProperty(const string& s)
{
    cout << "Getting planner property" << endl;
    for(const auto& p : this->plannerArguments) {
        if(p.first == s) {
            return p.second;
        }
    }
    return "";
}

bool tsfd::TemporalStressingPlannerController::setPlannerProperty(const string& var, const string& value)
{
    cout << "Setting planner property" << endl;
    this->plannerArguments.push_back(std::make_pair(var, value));
    cout << "Adding PlannerParameters " << var << " to " << value << ": " << endl;
    return true;
}

void tsfd::TemporalStressingPlannerController::clearDomain()
{
    cout << "Reset Domain" << endl;
    this->domain = "";
}

void tsfd::TemporalStressingPlannerController::clearDomainProblems()
{
    cout << "Reset Domain Problems" << endl;
    this->domainProblems = map<string, string>();
}

bool tsfd::TemporalStressingPlannerController::loadDomain(const string& file)
{
    cout << "Load Domain" << endl;
    ifstream ifs(file);
    string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
    return this->defineDomain(content);
}

bool tsfd::TemporalStressingPlannerController::loadDomainProblems(const string& file)
{
    cout << "Load Domain Problems" << endl;
    ifstream ifs(file);
    string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
    return this->defineDomainProblems("problem", content); // TODO
}

bool tsfd::TemporalStressingPlannerController::defineDomain(const string& str)
{
    cout << "Define Domain" << endl;
    this->domain = str;
    return true;
}

bool tsfd::TemporalStressingPlannerController::defineDomainProblems(const string& name, const string& str)
{
    cout << "Define Domain Problem" << endl;
    this->domainProblems.insert(make_pair(name, str));
    return true;
}

// Planning functions
void tsfd::TemporalStressingPlannerController::clearPlan()
{
    cout << "Clear Plan" << endl;
}

void tsfd::TemporalStressingPlannerController::resetPlan()
{
    cout << "Reset Plan" << endl;
}

bool tsfd::TemporalStressingPlannerController::buildPlan()
{
    cout << "Build Plan" << endl;
    // For safety first reset everything
    reset_everything();

    struct tms start, search_start, search_end;
    times(&start);
    double start_walltime, search_start_walltime, search_end_walltime;
    start_walltime = this->getCurrentTime();

    cout << "Using planner parameters" << endl;
    if(!g_parameters.readParameters(this->plannerArguments)) {
        cerr << "Error in reading parameters.\n";
        return false;
    }
    //g_parameters.dump();

    // store domain etc in file inside tmp folder
    string full_domain = this->domain;
    string full_problem = this->domainProblems.find(this->useProblem)->second;
    string domain_filepath = "/tmp/pddl2sas_input_domain.pddl";
    string problem_filepath = "/tmp/pddl2sas_input_problem.pddl";

    ofstream out_domain;
    out_domain.open(domain_filepath);
    ofstream out_problem;
    out_problem.open(problem_filepath);

    if(out_domain.fail()) {
        cerr << "Error creating pddl domain file with the following error: " << (errno) << " (EOE)" << endl;
        return false;
    }
    if(out_problem.fail()) {
        cerr << "Error creating pddl problem file with the following error: " << (errno) << " (EOE)" << endl;
        return false;
    }

    out_domain << full_domain;
    out_problem << full_problem;
    out_domain.close();
    out_problem.close();

    cout << "Using the domain: " << endl;
    cout << full_domain << endl;
    cout << "Using the problem: " << endl;
    cout << full_problem << endl;

    // preprocess data using python pddl2sas parser
    string sas_filepath = "/tmp/preprocessed_output.sas";
    string variables_filepath = "/tmp/sas_all.groups";

    string escaped_domain_filepath = "'" + domain_filepath + "'";
    string escaped_problem_filepath = "'" + problem_filepath + "'";

    std::string curr_loc = __FILE__;
    std::size_t curr_loc_pos = curr_loc.find("/TemporalStressingFastDownward/TemporalStressingFastDownward/");
    if(curr_loc_pos == string::npos) {
        cerr << "Someone changed the name structure of this project! Python file cannot be found anymore!" << endl;
        return false;
    }
    std::string base_loc = curr_loc.substr(0, curr_loc_pos) + "/TemporalStressingFastDownward";
    std::string etc_loc = base_loc + "/etc";

    string syscall = etc_loc + "/pddl2sas " + escaped_domain_filepath + " " + escaped_problem_filepath;

    int system_ret = system(syscall.c_str());
    if(system_ret == -1) {
        cerr << "System command failed with return value " << system_ret << ". " << endl;
        cerr << "Used command: " << syscall << endl;
        cerr << "Continue with sas file if there is one at /tmp/preprocessed_output.sas" << endl;
    } else {
        cout << "The system command returned " << system_ret << ". We assume that python compiler has been called successfully." << endl;
    }

    ifstream in(sas_filepath);

    bool poly_time_method = false;
    in >> poly_time_method;
    if(poly_time_method) {
        cout << "Poly-time method not implemented in this branch." << endl;
        cout << "Starting normal solver." << endl;
    }

    read_everything(in);
    //dump_everything();

    // Remove sas file
    //if(remove(sas_filepath.data()) != 0 ) {
    //  cerr << "Error deleting file at " << sas_filepath << endl;
    //  return false;
    //}
    //cout << "SAS File successfully deleted" << endl;

    //cout << "Contains universal conditions: " << g_contains_universal_conditions << endl;

    //exit(0);

    g_let_time_pass = new Operator(false);
    g_wait_operator = new Operator(true);

    ifstream vin(variables_filepath);
    this->pddl2sasManager = PddlToSasManager(vin);
    this->pddl2sasManager.dump();

    // create a new engine
    BestFirstSearchEngine* engine = new BestFirstSearchEngine();

    // send heuristics to engine
    if(g_parameters.makespan_heuristic || g_parameters.makespan_heuristic_preferred_operators) {
        engine->add_heuristic(new CyclicCGHeuristic(CyclicCGHeuristic::REMAINING_MAKESPAN), g_parameters.makespan_heuristic, g_parameters.makespan_heuristic_preferred_operators);
    }
    if(g_parameters.cyclic_cg_heuristic || g_parameters.cyclic_cg_preferred_operators) {
        engine->add_heuristic(new CyclicCGHeuristic(CyclicCGHeuristic::CEA), g_parameters.cyclic_cg_heuristic, g_parameters.cyclic_cg_preferred_operators);
    }
    if(g_parameters.no_heuristic) {
        engine->add_heuristic(new NoHeuristic, g_parameters.no_heuristic, false);
    }
    if(g_parameters.rtpg_heuristic || g_parameters.rtpg_heuristic_preferred_operators) {
        engine->add_heuristic(new RTPGHeuristic, g_parameters.rtpg_heuristic, g_parameters.rtpg_heuristic_preferred_operators);
    }
    if(g_parameters.known_plan_heuristic) {
        engine->add_heuristic(new ScrambledEggHeuristic, g_parameters.known_plan_heuristic, false);
    }

    this->bestMakespan = REALLYBIG;
    this->bestCost = REALLYBIG;

    times(&search_start);
    search_start_walltime = this->getCurrentTime();

    SearchEngine::status search_result = SearchEngine::IN_PROGRESS;

    // set current state to initial state
    assert(g_initial_state != NULL);
    this->currentState = TimeStampedState(*g_initial_state);

    if(g_parameters.max_makespan >= 0) {
        engine->maximumMakespan = g_parameters.max_makespan;
    }

    if(g_parameters.max_cost >= 0) {
        engine->maximumCost = g_parameters.max_cost;
    }

    cout << ">>> Starting search <<<" << endl;
    while(true) {
        engine->initialize();
        search_result = engine->search();

        times(&search_end);
        search_end_walltime = getCurrentTime();
        if(engine->found_solution()) {
            cout << "Solution has been found." << endl;
            if(search_result == SearchEngine::SOLVED) {
                std::pair<double, double> best_mc = this->update_plan(*engine, this->bestMakespan, this->bestCost);
                this->bestMakespan = best_mc.first;
                this->bestCost = best_mc.second;
                engine->bestMakespan = this->bestMakespan;
                engine->bestCost = this->bestCost;
            }

            // to continue searching we need to be in anytime search and the ret value is SOLVED
            // all other possibilities are either a timeout or completely explored search space
            if(g_parameters.anytime_search) {
                if (search_result == SearchEngine::SOLVED) {
                    engine->fetch_next_state();
                } else {
                    break;
                }
            } else {
                break;
            }
        } else {
            break;
        }
    }

    cout << "------------ End of planning ------------ " << endl;
    cout << "Engine planned in total " << engine->getNumberOfSearchSteps() << " steps." << endl;
    cout << "Found best makespan: " << this->bestMakespan << ", Found best cost: " << this->bestCost << endl;

    double search_time_wall = search_end_walltime - search_start_walltime;
    double total_time_wall = search_end_walltime - start_walltime;

    unsigned long search_ms = (search_end.tms_utime - search_start.tms_utime) * 10;
    unsigned long total_ms = (search_end.tms_utime - start.tms_utime) * 10;
    double search_time = 0.001 * (double)search_ms;
    double total_time = 0.001 * (double)total_ms;
    cout << "Search time: " << search_time << " seconds - Walltime: " << search_time_wall << " seconds" << endl;
    cout << "Total time: " << total_time << " seconds - Walltime: " << total_time_wall << " seconds" << endl;

    delete(engine);

    switch(search_result) {
        case SearchEngine::SOLVED_TIMEOUT:
        case SearchEngine::SOLVED:
        case SearchEngine::SOLVED_COMPLETE:
            return true;
        case SearchEngine::FAILED_TIMEOUT:
        case SearchEngine::FAILED:
            return false;
        default:
            cerr << "Invalid Search return value: " << search_result << endl;
    }
    return false;
}

Plan tsfd::TemporalStressingPlannerController::getPlan()
{
    cout << "Get Plan" << endl;
    return this->plan;
}

TimeStampedState tsfd::TemporalStressingPlannerController::getPlanState()
{
    cout << "Get Plan State" << endl;
    return this->currentState;
}

PlanStep tsfd::TemporalStressingPlannerController::getPlanAction()
{
    cout << "Get Plan Action" << endl;
    if(this->isEndOfPlan()) {
        return PlanStep();
    }

    assert(this->nextPlanStep < this->plan.size());
    return this->plan[this->nextPlanStep];
}

double tsfd::TemporalStressingPlannerController::getStartTimeOfOvernextPlanAction()
{
    cout << "Get Start overnext" << endl;
    PlanStep plan_step;
    this->advancePlan();
    plan_step = this->getPlanAction();
    this->decrementPlan();

    return plan_step.start_time;
}

bool tsfd::TemporalStressingPlannerController::isPlanDefined()
{
    cout << "is Plan Defined" << endl;
    return this->plan.size() > 0;
}

bool tsfd::TemporalStressingPlannerController::isEndOfPlan()
{
    cout << "check is EOP" << endl;
    return this->nextPlanStep >= this->plan.size();
}

void tsfd::TemporalStressingPlannerController::advancePlan()
{
    cout << "Advance" << endl;
    this->nextPlanStep = min(this->nextPlanStep+1, this->plan.size());
    cout << "Next Plan Step is " << this->nextPlanStep << " of " << this->plan.size() << endl;
}

void tsfd::TemporalStressingPlannerController::decrementPlan()
{
    cout << "Decrement" << endl;
    this->nextPlanStep = max(this->nextPlanStep-1, (size_t) 0);
    cout << "Next Plan Step is " << this->nextPlanStep << " of " << this->plan.size() << endl;
}

bool tsfd::TemporalStressingPlannerController::setPlanProblem(const string& s)
{
    cout << "Set Problem" << endl;
    this->useProblem = s;
    return false;
}

bool tsfd::TemporalStressingPlannerController::checkPlanActionPreconditions(const TimeStampedState& s)
{
    cout << "Check Preconds" << endl;
    assert(this->plan.size() > 0);
    assert(this->plan.size() >= this->nextPlanStep);

    //cout << "check whether next action is applicable in state" << endl;
    //this->getPlanAction().op->dump();
    //s.dump(true);

    cout << " -------------- " << endl;

    if(this->getPlanAction().op->is_applicable(s)) {
        cout << "Plan Action " << this->getPlanAction().op->get_name() << " is applicable" << endl;
        return true;
    }
    cout << "Plan Action " << this->getPlanAction().op->get_name() << " is not applicable" << endl;
    return false;
}

SASAtom tsfd::TemporalStressingPlannerController::getSASForPDDLFact(const PDDLAtom& pddl) const {
    // sas is of form '<int, vector<double>>'
    // IMPORTANT <-1, <>> means not found
    SASAtom ret = this->pddl2sasManager.getSasForPddl(pddl);
    return ret;
}

PDDLAtom tsfd::TemporalStressingPlannerController::getPDDLForSASVariable(const SASAtom& sas) const {
    // pddl is of form 'predicate fact fact ...'
    // IMPORTANT "" means not found
    PDDLAtom ret = this->pddl2sasManager.getPddlForSas(sas);
    return ret;
}

int tsfd::TemporalStressingPlannerController::getStateSize() const {
    return g_variable_name.size();
}

Operator tsfd::TemporalStressingPlannerController::getTemporalOperatorForName(const string& name) const {
    for(const auto& top : g_operators) {
        if(top.get_name() == name)
            return top;
    }
    return Operator();
}

/*
 * Returns the StateChange from s timestamp up to next scheduling node
 */
TimeStampedStateChange tsfd::TemporalStressingPlannerController::getPlanActionEffects(const TimeStampedState& s)
{
    assert(this->checkPlanActionPreconditions(s));

    TimeStampedState temp = TimeStampedState(s, *(this->getPlanAction().op));
    //cout << "STATE" << endl;
    //temp.dump(true);

    double startOfOvernextAction = this->getStartTimeOfOvernextPlanAction();

    // This is useful for concurrent plans
    if(startOfOvernextAction < 0) { // means there is no next action
        temp = temp.let_time_pass(temp.next_happening() - temp.timestamp, true);
    } else {
        temp = temp.let_time_pass(startOfOvernextAction - s.timestamp, true);
    }

    //cout << "STATE AFTER APPLYING " << *(this->getPlanAction().op)->get_name() << endl;
    //temp.dump();

    return TimeStampedStateChange(s, temp);
}

TimeStampedState tsfd::TemporalStressingPlannerController::getPlanActionEffectsState(const TimeStampedState& s)
{
    //StateFactList e_ice;
    /*vector<pks::StateProperty> s_pks, e_pks;

    statePropertyListIceToPks(s, s_pks);

    planner->getPlanActionEffectsState(s_pks, e_pks);

    statePropertyListPksToIce(e_pks, e_ice);
    */
    return TimeStampedState();
}

TimeStampedState tsfd::TemporalStressingPlannerController::getPlanInitialState()
{
    if(g_initial_state == NULL) {
        return TimeStampedState();
    }
    return *g_initial_state;
}

TimeStampedState tsfd::TemporalStressingPlannerController::getDomainInitialState()
{
    if(g_initial_state == NULL) {
        return TimeStampedState();
    }
    return *g_initial_state;
}

TimeStampedStateChange tsfd::TemporalStressingPlannerController::getStateDifference(const TimeStampedState& s1_ice, const TimeStampedState& s2_ice)
{
    TimeStampedStateChange diff;
    /*vector<spoac::StateProperty> add_ice, del_ice;
    vector<pks::StateProperty> s1_pks, s2_pks, add_pks, del_pks;

    statePropertyListIceToPks(s1_ice, s1_pks);
    statePropertyListIceToPks(s2_ice, s2_pks);

    planner->getStateDifference(s1_pks, s2_pks, add_pks, del_pks);

    statePropertyListPksToIce(add_pks, add_ice);
    statePropertyListPksToIce(del_pks, del_ice);

    diff.addList = add_ice;
    diff.deleteList = del_ice;
    */
    return diff;
}

// String functions
vector<string> tsfd::TemporalStressingPlannerController::timeStampedStateToString(const TimeStampedState& p)
{
    vector<string> v;
    /*
    for (unsigned i = 0 ; i < p.size() ; i++)
    {
        v.push_back(statePropertyToString(p[i], c));
    }
    */
    return (v);
}

string tsfd::TemporalStressingPlannerController::stateAssignmentToString(const pair<int, double>& p)
{
    ostringstream s;
    /*
    if (p.value.empty() == false)
    {
        if (p.name.empty())
        {
            s << "-empty-";
        }
        else
        {
            s << p.name;
        }
        s << "(";
        for (unsigned i = 0 ; i < p.args.size() ; i++)
        {
            s << p.args[i];
            if (i < (p.args.size() - 1))
            {
                s << ",";
            }
        }
        s << ") ";

        if (p.sign == false)
        {
            s << "!";
        }
        s << "= ";
        s << p.value;
    }
    else
    {
        if (p.sign == false)
        {
            s << "!";
        }
        if (p.name.empty())
        {
            s << "-empty-";
        }
        else
        {
            s << p.name;
        }
        if (p.args.size() > 0)
        {
            s << "(";
            for (unsigned i = 0 ; i < p.args.size() ; i++)
            {
                s << p.args[i];
                if (i < (p.args.size() - 1))
                {
                    s << ",";
                }
            }
            s << ")";
        }
    }
    */
    return (s.str());
}

vector<string> tsfd::TemporalStressingPlannerController::planToString(const Plan& p)
{
    vector<string> v;
    /*
    for (unsigned i = 0 ; i < p.size() ; i++)
    {
        v.push_back(PlanStepToString(p[i], c));
    }
    */
    return (v);
}

string tsfd::TemporalStressingPlannerController::planStepToString(const PlanStep& p)
{
    ostringstream s;
    /*
    if (p.name.empty() && p.type.empty() && p.args.size() == 0)
    {
        s << "(empty)";
    }
    else
    {
        if (p.name.empty())
        {
            s << "-empty-";
        }
        else
        {
            s << p.name;
        }
        if (p.args.size() > 0)
        {
            s << "(";
            for (unsigned i = 0 ; i < p.args.size() ; i++)
            {
                s << p.args[i];
                if (i < (p.args.size() - 1))
                {
                    s << ",";
                }
            }
            s << ")";
        }

        if (p.type.empty() == false)
        {
            s << " : " << p.type;
        }
    }
    */
    return (s.str());
}

double tsfd::TemporalStressingPlannerController::getCurrentTime()
{
    const double USEC_PER_SEC = 1000000;
    
    struct timeval tv;
    gettimeofday(&tv, 0);
    return(tv.tv_sec + (double)tv.tv_usec / USEC_PER_SEC);
}

std::pair<double, double> tsfd::TemporalStressingPlannerController::update_plan(BestFirstSearchEngine& engine, double best_makespan, double best_cost)
{
    const vector<PlanStep> &plan = engine.get_plan();

    assert(engine.current_state.satisfies(g_goal));

    double makespan = engine.current_state.timestamp;
    double cost = engine.current_state.cost;

    if(makespan >= g_parameters.max_makespan) {
        return std::make_pair(best_makespan, best_cost);
    }
    if(g_parameters.g_values == PlannerParameters::GMakespan && makespan >= best_makespan) {
        return std::make_pair(best_makespan, best_cost);
    }
    if(g_parameters.g_values == PlannerParameters::GCost && cost >= best_cost) {
        return std::make_pair(best_makespan, best_cost);
    }

    cout << endl << "Found new plan:" << endl;
    for(unsigned int i = 0; i < plan.size(); i++) {
        const PlanStep& step = plan[i];
        cout << step.start_time << ": " << step.op->get_name() << " , duration = " << step.op->initial_duration << ", cost = " << step.op->initial_cost << endl;
    }

    // Store new plan
    this->plan = plan;

    cout << "Plan length: " << plan.size() << " step(s)." << endl;
    cout << "Plan Makespan (ignoring no-moving-targets-rule): " << makespan ;
    cout << endl;

    cout << "Plan Cost: " << cost;
    cout << endl;


    return std::make_pair(makespan, cost);
}
