#pragma once

// ATTENTION!!!
// This is an informed heuristic and therefore only useful for the H2T demonstation (to reduce planning time)
// REMOVE THIS AFTER DEMO

#include "heuristic.h"


namespace tsfd {

class TimeStampedState;

class ScrambledEggHeuristic : public Heuristic
{
        enum {
            QUITE_A_LOT = 1000000
        };
    protected:
        virtual void initialize();
        virtual double compute_heuristic(const TimeStampedState &TimeStampedState);

        std::vector<TimeStampedState> plan_states;
    public:
        ScrambledEggHeuristic() {}
        ~ScrambledEggHeuristic() {}
        virtual bool dead_ends_are_reliable() {
            return true;
        }
};

}
