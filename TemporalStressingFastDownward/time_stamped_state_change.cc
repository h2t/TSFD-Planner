//
//  real_time_stamped_state_change.cpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#include "time_stamped_state_change.h"

#include <algorithm>

using namespace std;
using namespace tsfd;

TimeStampedStateChange::TimeStampedStateChange(const TimeStampedState& a, const TimeStampedState& b) :
        timestamp_a(a.timestamp), timestamp_b(b.timestamp), cost_a(a.cost), cost_b(b.cost)
{
    // create diffs in state assignment
    for(unsigned int i = 0; i < a.state.size(); ++i) {
        vector<pair<int, double> > assignment;
        assignment.push_back(make_pair(i, a[i]));
        if(!b.satisfies(assignment)) {
            this->stateChanges.push_back(PrePost(i, a[i], i, b[i]));
        }
    }

    // add scheduled operators
    for(const auto& scheduled_op : b.operators) {
        if(std::find(a.operators.begin(), a.operators.end(), scheduled_op) == a.operators.end()) {
            this->addedScheduledOperators.push_back(scheduled_op);
        }
    }

    // del scheduled operators
    for(const auto& scheduled_op : a.operators) {
        if(find(b.operators.begin(), b.operators.end(), scheduled_op) == b.operators.end()) {
            this->removedScheduledOperators.push_back(scheduled_op);
        }
    }
}

TimeStampedStateChange::TimeStampedStateChange(const TimeStampedStateChange& tssc) :
        stateChanges(tssc.stateChanges), addedScheduledOperators(tssc.addedScheduledOperators), removedScheduledOperators(tssc.removedScheduledOperators), timestamp_a(tssc.timestamp_a), timestamp_b(tssc.timestamp_b), cost_a(tssc.cost_a), cost_b(tssc.cost_b)
{}
