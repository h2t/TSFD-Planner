
#pragma once

#include "heuristic.h"

#include <set>
#include <vector>
#include <tr1/unordered_map>
#include <tr1/tuple>

#include "closed_list.h"

namespace tsfd {

class TimeStampedState;
class Operator;
struct Prevail;
struct PrePost;

class RTPGHeuristic : public Heuristic
{
    typedef std::tr1::unordered_map<TimeStampedState, double, TssHash, TssEquals> HeuristicValueCache;
    HeuristicValueCache cacheHeuristicValue;

    typedef std::tr1::unordered_map<TimeStampedState, std::vector<std::set<const Operator*> >, TssHash, TssEquals> PrefOpsCache;
    PrefOpsCache cachePrefOps;

    typedef std::tr1::unordered_map<TimeStampedState, std::vector<double>, TssHash, TssEquals> CostsToGoalCache;
    CostsToGoalCache cacheCostsToGoal;

    protected:
        virtual void initialize();
        virtual double compute_heuristic(const TimeStampedState &timeStampedState);
    public:
        RTPGHeuristic() {}
        ~RTPGHeuristic() {}
        virtual bool dead_ends_are_reliable() {
            return true;
        }
        virtual bool only_insert_future_and_past() {
            return false;
        }
private:
        const double VALUE_NOT_SET = -1;

        std::set<int> cheapestGoals;
        std::set<int> mostExpensiveGoals;
        std::set<int> randGoals;
        std::vector<double> costsOfGoals;
        std::vector<std::set<const Operator*> > prefOpsSortedByCorrespondigGoal;

        std::vector<std::vector<std::pair<double, double> > > current_state_numeric_set_at;
        std::vector<std::vector<double> > current_state_set_at;
        std::vector<std::vector<const Operator*> > set_by;


        bool is_applicable(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const Prevail&) const;
        bool is_applicable(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const PrePost&) const;
        bool is_applicable(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const Operator&) const;

        double is_applicable_at(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const Prevail&) const;
        double is_applicable_at(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const PrePost&) const;

        bool apply_effects_to_state(std::vector<std::vector<double> >& state, std::vector<std::vector<std::pair<double, double> > >& numeric_state, std::vector<std::vector<const Operator*> >& set_by, const std::vector<PrePost>& preposts, const Operator& op, double val) const;
        double get_startvalue_of_op(const std::vector<std::vector<double> >&, const std::vector<std::vector<std::pair<double, double> > >&, const Operator&) const;
        bool goal_reached() const;

        bool compute_goal_fulfilling_assignment(const TimeStampedState &timeStampedState);
        double compute_heuristic_value_of_relaxed(const TimeStampedState& state);

        std::vector<const Operator*> get_all_operators_that_causes_assignment(const std::pair<int, double>& ass, vector<const Operator*>& already_checked) const;

        void compute_pref_ops(double heuristic, const TimeStampedState &state);
        void setAllPrefOpsExternally();
        void setFirstPrefOpsExternally(unsigned int number);
        void setConcurrentPrefOpsExternally(const TimeStampedState &state);
        void setRandPrefOpsExternally(unsigned int number);
        void setMostExpensivePrefOpsExternally(unsigned int number);
        void setCheapestPrefOpsExternally(unsigned int number);
        void set_specific_pref_ops(const TimeStampedState &state);
        void reset_pref_ops_from_cache(double heuristic, const TimeStampedState &state);
};

}
