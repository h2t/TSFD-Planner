
#pragma once

#include <iostream>
#include <vector>


namespace tsfd {

    class Operator;
    class TimeStampedState;

    class SuccessorGenerator
    {
        public:
            virtual ~SuccessorGenerator()
            {
            }
            virtual void generate_applicable_ops(const TimeStampedState &curr,
                    std::vector<const Operator *> &ops) = 0;
            void dump()
            {
                _dump("  ");
            }
            virtual void _dump(string indent) = 0;
    };

    SuccessorGenerator *read_successor_generator(std::istream &in);

}
