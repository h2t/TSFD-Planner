//
//  pddl_to_sas_manager.hpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <memory>


namespace tsfd {

class PddlToSasManager;
class PDDLAtom;
class SASAtom;

class PDDLAtom {
public:
    bool negated = false;
    std::string predicate = "";
    std::vector<std::string> args;

    PDDLAtom() {}
    PDDLAtom(bool, const std::string&, const std::vector<std::string>&);
    PDDLAtom(std::istream&);
    PDDLAtom(const PDDLAtom&);

    bool valid() const;

    std::string toString() const;
    void dump() const;

    bool operator<(const PDDLAtom& a) const {
        return this->toString() < a.toString();
    }
};

class SASAtom {
public:
    enum {
        NO_VARIABLE = -1
    };
    int index = -1;
    int value = -1;

    SASAtom() {}
    SASAtom(int, int);
    SASAtom(std::istream&);
    SASAtom(const SASAtom&);

    bool valid() const;

    std::string toString() const;
    void dump() const;

    bool operator<(const SASAtom& a) const {
        return this->toString() < a.toString();
    }
};

class PddlToSasManager
{
public:
    PddlToSasManager() {}

    PddlToSasManager(std::istream&);
    SASAtom getSasForPddl(const PDDLAtom&) const;
    PDDLAtom getPddlForSas(const SASAtom& sas) const;
    void dump() const;

private:
    void insertPddlToSas(const PDDLAtom& pddl, const SASAtom& sas);
    void insertSasToPddl(const SASAtom& sas, const PDDLAtom& pddl);

private:
    std::map<PDDLAtom, SASAtom> pddl_to_sas;
    std::map<SASAtom, PDDLAtom>  sas_to_pddl;
};
}
