
#pragma once

#include <string>
using namespace std;
#include "best_first_search.h"


namespace tsfd {

class PlannerParameters
{
   public:
      PlannerParameters();
      ~PlannerParameters();
 
      /// Read parameters from ROS and cmd line (cmd line overrides ROS parameters).
      bool readParameters(std::vector<std::pair<std::string, std::string> >&);

      void dump() const;

   public:
      bool anytime_search;          ///< Perform anytime search (don't stop at first plan)
      
      int timeout_if_plan_found;          ///< Timeout if a plan was found (0 - inf).
      int timeout_while_no_plan_found;    ///< Timeout while no plan found (0 - inf).

      int open_list_initial_boost;
      int open_list_boost;
      
      bool greedy;                  ///< Perform greedy search
      bool lazy_evaluation;         ///< Lazy heuristic evaluation
      bool verbose;                 ///< Verbose outputs

      bool cyclic_cg_heuristic;                    ///< Use cyclic_cg heuristic
      bool cyclic_cg_preferred_operators;          ///< Use cyclic_cg heuristic preferred operators
      bool makespan_heuristic;                     ///< Use makespan heuristic
      bool makespan_heuristic_preferred_operators; ///< Use makespan heuristic preferred operators
      bool no_heuristic;                           ///< Use the no heuristic
      bool rtpg_heuristic;                         ///< Use the rtpg heuristic
      bool rtpg_heuristic_preferred_operators;     ///< Use the rtpg heuristic preferred operators
      bool known_plan_heuristic;                   ///< Use a heuristic which knows the real plan. Only scheduling enabled

      bool cg_heuristic_zero_cost_waiting_transitions;  ///< If false, scheduled effects are accounted
      bool cg_heuristic_fire_waiting_transitions_only_if_local_problems_matches_state;

      int discretizations = 1;

      bool future_scheduling;
      bool past_scheduling;

      double max_makespan;
      double max_cost;
 
      /// Possible definitions of "g"
      enum GValues {
         GMakespan,               ///< g values by makespan (timestamp + longest duration of running operators
         GCost                   ///< g values by path cost
      };
      enum GValues g_values;      ///< How g values are calculated - Default: Timestamp

      enum CostCalculation {
          ADD,
          MULTIPLY
      };

      CostCalculation costCalculation;

      bool pref_ops_ordered_mode;
      int number_pref_ops_ordered_mode;
      bool pref_ops_cheapest_mode;
      int number_pref_ops_cheapest_mode;
      bool pref_ops_most_expensive_mode;
      int number_pref_ops_most_expensive_mode;
      bool pref_ops_rand_mode;
      int number_pref_ops_rand_mode;
      bool pref_ops_concurrent_mode;

      bool epsilonize_internally;   ///< add eps_time when applying an operator
      bool epsilonize_externally;   ///< Add epsilon steps in between plan steps by calling epsilonize_plan.

      string planMonitorFileName;   ///< Filename for monitoring (if set, implies monitoring mode)

      bool monitoring_verify_timestamps;     ///< During monitoring only accept the monitored plan if the timestamps match the original one.
   
   protected:
      /// Read parameters from command line.
      bool readParameterMap(std::vector<std::pair<std::string, std::string> >&);
};

}
